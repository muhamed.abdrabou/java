FROM openjdk:11-jre-slim

# Set the working directory in the container
WORKDIR /app

# Copy the executable JAR file to the working directory
COPY build/libs/app.jar app.jar

# Run the JAR file
ENTRYPOINT ["java", "-jar", "app.jar"]