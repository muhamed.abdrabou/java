# Simple Java Application

This is a simple Java application that prints "Hello, World!".

## How to Build

To build the application, run the following command:

sh
./gradlew build


## How to Run Tests

To run the tests, use the following command:

sh
./gradlew test


## How to Deploy

The application is deployed using AWS CLI commands in the CI/CD pipeline.

### Deployment Environments

- Dev: Development environment
- Test: Testing environment
- Stg: Staging environment

## Project Structure


.
├── src
│   └── main
│       └── java
│           └── App.java
├── .gitignore
├── build.gradle
└── settings.gradle


- `src/main/java/App.java`: Main Java application file.
- `.gitignore`: Specifies files and directories to be ignored by Git.
- `build.gradle`: Gradle build configuration.
- `settings.gradle`: Gradle settings.

## CI/CD Pipeline for Simple Java Application

This repository uses GitLab CI/CD to build, test, and deploy the simple Java application.

### Pipeline Stages

The pipeline consists of the following stages:

- **Build**: Compiles the Java application.
- **Test**: Runs the unit tests.
- **Deploy**:
  - **Dev**: Deploys to the development environment on pushes to the `develop` branch.
  - **Test**: Deploys to the testing environment after a successful deployment to the development environment.
  - **Stg**: Deploys to the staging environment on merges to the `master` branch.

### Configuration

#### AWS Configuration

Ensure that your AWS credentials are configured in GitLab CI/CD environment variables:

- `AWS_REGION`: Your AWS region.
- `AWS_ACCESS_KEY_ID`: Your AWS access key ID.
- `AWS_SECRET_ACCESS_KEY`: Your AWS secret access key.

#### Buckets Configuration

Specify your S3 buckets for each environment in the `.gitlab-ci.yml` file:

- `DEV_BUCKET`: Your development S3 bucket.
- `TEST_BUCKET`: Your testing S3 bucket.
- `STG_BUCKET`: Your staging S3 bucket.

#### `.gitlab-ci.yml` Configuration

Here's an example of the `.gitlab-ci.yml` file used for the CI/CD pipeline:

yaml
stages:
  - build
  - test
  - deploy

variables:
  AWS_REGION: "your-aws-region"
  AWS_ACCESS_KEY_ID: "your-access-key-id"
  AWS_SECRET_ACCESS_KEY: "your-secret-access-key"
  DEV_BUCKET: "your-dev-bucket"
  TEST_BUCKET: "your-test-bucket"
  STG_BUCKET: "your-stg-bucket"

before_script:
  - echo "Setting up environment..."
  - apt-get update -qq && apt-get install -qq -y jq
  - export PATH=$PATH:/usr/local/bin
  - curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
  - unzip awscliv2.zip
  - sudo ./aws/install
  - aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
  - aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
  - aws configure set region $AWS_REGION

build:
  stage: build
  script:
    - echo "Building the application..."
    - ./gradlew build

test:
  stage: test
  script:
    - echo "Running tests..."
    - ./gradlew test

deploy_dev:
  stage: deploy
  script:
    - echo "Deploying to Dev environment..."
    - aws s3 cp build/libs/app.jar s3://$DEV_BUCKET/app.jar
  only:
    - develop

deploy_test:
  stage: deploy
  script:
    - echo "Deploying to Test environment..."
    - aws s3 cp build/libs/app.jar s3://$TEST_BUCKET/app.jar
  only:
    - develop
  dependencies:
    - deploy_dev

deploy_stg:
  stage: deploy
  script:
    - echo "Deploying to Staging environment..."
    - aws s3 cp build/libs/app.jar s3://$STG_BUCKET/app.jar
  only:
    - master


### Protected Branches

- **Master**: Only maintainers or owners can merge to this branch.

### How to Trigger Pipeline

The pipeline is triggered automatically on commits and merge requests to the `develop` and `master` branches.

### Deployment Logic

Deployment is handled directly in the CI/CD pipeline using AWS CLI commands.

sh
aws s3 cp build/libs/app.jar s3://your-bucket/app.jar


### Additional Steps

<<<<<<< HEAD
Configure AWS Credentials: Ensure that your AWS credentials are configured in GitLab CI/CD environment variables for deployment to AWS.
=======
Configure AWS Credentials: Ensure that your AWS credentials are configured in GitLab CI/CD environment variables for deployment to AWS.
>>>>>>> master
