# Java Application Containerization and Deployment

## Containerizing the Java Application

### Build and Push Docker Image

Build the Docker image and push it to Docker Hub:

```sh
docker build -t <your-dockerhub-username>/java-app:latest .
docker push <your-dockerhub-username>/java-app:latest
```

## Run Docker Compose

Run the Docker Compose to start the application:

```sh
docker-compose up
```

## Kubernetes Deployment for Java Application

Don't forget to replace "your-dockerhub-username" inside k8s-deployment.yaml

### Apply Kubernetes Deployment and Service

Apply the Kubernetes YAML manifests to deploy the application:

```sh
kubectl apply -f k8s-deployment.yaml
```